package com.am.beautiful.app4.blouse.designs

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
